const {
    securityContext: {
        appId
    }
} = COMPILE_CONTEXT;

cube(`ReturningUsers`, {
    sql: `SELECT user_id, min(first_timestamp) AS first_timestamp, COUNT(*) AS session_count FROM ${appId}.sessions AS e GROUP BY e.user_id`,
    measures: {
        allSessionsCount: {
            sql: `session_count`,
            type: `count`
       },
        returningUserCount: { // Must be used with returningUsers segment
            sql: `session_count`,
            type: `count`,
            filters: [{ sql: `session_count >= 2` }],
            public: false
        },
        returningUserPercentage: {
            sql: `100 * ${returningUserCount} / ${allSessionsCount}`,
            type: `number`
        }
    },
    segments: {
        returningUsers: {
            sql: `session_count >= 2`,
        },
    },
    dimensions: {
        first_timestamp: {
            sql: `first_timestamp`,
            type: `time`
        }
    }
});