const {
  securityContext: {
    appId
  }
} = COMPILE_CONTEXT;

cube(`Sessions`, {
  sql: `SELECT domain_sessionid, 
    min(first_timestamp) as first_timestamp, 
    max(last_timestamp) as last_timestamp,
    any(user_id) as user_id,
    any(agent_name) as agent_name,
    any(os_name) as os_name,
    any(os_version) as os_version,
    any(os_version_major) as os_version_major,
    any(agent_version) as agent_version,
    any(browser_language) as browser_language,
    any(viewport_size) as viewport_size
  FROM ${appId}.sessions 
    GROUP BY domain_sessionid`,
  measures: {
    count: {
      sql: `domain_sessionid`,
      type: `count`,
    },
    averagePerUser: {
      sql: `${count} / ${CUBE.uniqueUsersCount}`,
      type: `number`
    },
    averageDurationMinutes: {
      type: `avg`,
      sql: `(${endAt} - ${startAt}) / 60`
    },
    uniqueUsersCount: {
      type: `countDistinct`,
      sql: `user_id`
    },
    usersCount: {
      type: `count`,
      sql: `user_id`
    }
  },
  dimensions: {
    startAt: {
      sql: `first_timestamp`,
      type: `time`
    },
    userId: {
      sql: `user_id`,
      type: `string`,
    },
    sessionID: {
      sql: `domain_sessionid`,
      type: `string`,
      primary_key: true
    },
    endAt: {
      sql: `last_timestamp`,
      type: `time`
    },
    agentName: {
      sql: `agent_name`,
      type: `string`
    },
    osFamily: {
      sql: `os_family`,
      type: `string`
    },
    osName: {
      sql: `os_name`,
      type: `string`
    },
    osVersion: {
      sql: `os_version`,
      type: `string`
    },
    osVersionMajor: {
      sql: `os_version_major`,
      type: `string`
    },
    agentVersion: {
      sql: `agent_version`,
      type: `string`
    },
    browserLanguage: {
      sql: `browser_language`,
      type: `string`
    },
    documentLanguage: {
      sql: `document_language`,
      type: `string`
    },
    viewportSize: {
      sql: `viewport_size`,
      type: `string`
    }
  }
});
