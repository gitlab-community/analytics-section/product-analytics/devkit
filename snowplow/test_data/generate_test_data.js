const fs = require('fs');
const new_app_key = process.argv[2];

if (new_app_key == '-h' || new_app_key == '--help') {
    print_help_text();
}

if (!new_app_key) {
    console.log("Please provide new app key\n")
    print_help_text();
}

const number_of_days = parseInt(process.argv[3]) || 7;
const out_filename = process.argv[4] || 'new_test_data.csv';

try {
    fs.unlinkSync(out_filename);
    console.log(`${out_filename} removed, creating new data...`)
} catch {
    console.log(`${out_filename} does not exist, creating new data...`)
}

const TODAY = new Date();

var orig_data = fs.readFileSync('orig_test_data.csv').toString();

var headers = orig_data.substring(0, orig_data.indexOf('\n'));
// remove last newline from original data
orig_data = orig_data.substring(orig_data.indexOf('\n'), orig_data.length - 1);

fs.writeFileSync(out_filename, headers, { flag: 'a+' });

for (var day = 0; day < number_of_days; day++) {
    var fake_today = new Date(TODAY.getFullYear(), TODAY.getMonth(), TODAY.getDate() - day);
    var fake_today_string = `${fake_today.getFullYear()}-${(fake_today.getMonth()+1).toLocaleString(undefined, {minimumIntegerDigits: 2})}-${fake_today.getDate().toLocaleString(undefined, {minimumIntegerDigits: 2})}`;
    var fake_today_data = orig_data.replaceAll('2023-06-14', fake_today_string);

    for (var hour = 0; hour < 24; hour++) {
        var new_data = fake_today_data.replaceAll('19:', `${hour.toLocaleString(undefined, {minimumIntegerDigits: 2})}:`);
        fs.writeFileSync(out_filename, new_data, { flag: 'a+' });
    }
}

console.log(`New data data has been written to ${out_filename}\n`);
console.log(`You can import this into clickhouse using clickhouse-client.\
Example for standard devkit install:\
cat ${out_filename} | clickhouse-client --host 'localhost' --port 9000 --user test --password test --query 'insert into [TABLE_NAME].snowplow_events FORMAT CSVWithNames'`);

function print_help_text() {
    console.log("USAGE: node generate_test_data.js NEW_APP_KEY [NUMBER_OF_DAYS=7] [OUT_FILE=new_test_data.csv]\n")
    console.log("This will repeat the test data in 'orig_test_data.txt' 24 times a day\
(once an hour) for the specified number of days (default 7) in the past starting today\
to OUT_FILE (default new_test_data.csv)")
    process.exit()
}
