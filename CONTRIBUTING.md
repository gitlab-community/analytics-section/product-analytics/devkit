By contributing to GitLab B.V., You accept and agree to the following terms and conditions for Your present and future Contributions submitted to GitLab B.V. Except for the license granted herein to GitLab B.V. and recipients of software distributed by GitLab B.V., You reserve all right, title, and interest in and to Your Contributions. All Contributions are subject to the following DCO + License terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

<!-- This notice should stay as the first item in the CONTRIBUTING.md file.* -->

# How to contribute to DevKit

~"group::product analytics" is a brand new group at GitLab and so we're aiming to have a light-touch review process
  until the team becomes more mature. Before merging new changes to the devkit, the following conditions must be satisfied:

1. At least one other engineer or engineering manager in the group must approve the proposed changes.
2. The MR description must include a section describing what _could_ break for existing users of the devkit.
3. The [README](https://gitlab.com/gitlab-org/analytics-section/product-analytics/devkit/-/blob/main/README.md) must be kept updated with any new or changed steps to get up and running. 
   * New engineers will be joining over time and their time-to-onboard must be kept as low as possible.
4. Changes that will break the current setup for all existing group members **must** also be communicated internally in the
  #g_analyze_product_analytics Slack channel.

## Things to keep in mind

* It is hard to balance the requirement to have a bias-for-action against affecting others' productivity by merging 
breaking changes. 
  * When in doubt **lean towards having a bias-for-action** rather than waiting
    for approval from everyone in the group.
* The team is still quite small. Right now, communication across _everyone_ in the team is less
  burdensome than slowing velocity.