insert into gitlab_project_20001.snowplow_events (derived_tstamp, event, user_id, user_id_type, page_urlpath)
values ('2023-02-07 10:44:10','page_view', '1', 'identify', '/page1.html'),
       ('2023-02-07 10:44:12','page_view', '1', 'identify', '/page2.html'),
       ('2023-02-08 10:45:00','page_view', '2', 'identify', '/page1.html'),
       ('2023-02-08 10:45:00','page_view', '3', 'identify', '/page2.html'),
       ('2023-02-10 10:44:22','page_view', generateUUIDv4(), 'cookie', null),
       ('2023-02-11 10:44:22','page_view', generateUUIDv4(), 'cookie', null),
       ('2023-02-12 10:44:22','page_view', generateUUIDv4(), 'anonymous', null),
       ('2023-02-13 10:44:22','page_view', generateUUIDv4(), 'anonymous', null),
       ('2023-02-14 10:44:22','page_view', generateUUIDv4(), 'anonymous', null),
       ('2023-02-15 10:44:22','page_view', generateUUIDv4(), 'anonymous', null),
       ('2023-02-16 10:44:22','page_view', generateUUIDv4(), 'anonymous', null),
       ('2023-02-07 10:44:22','page_view', generateUUIDv4(), 'anonymous', null);

