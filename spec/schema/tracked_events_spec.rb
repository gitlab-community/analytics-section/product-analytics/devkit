# frozen_string_literal: true

RSpec.describe 'TrackedEvents' do
  it 'returns the correct number of tracked pageviews' do
    response = cube_query({ "measures": ['TrackedEvents.pageViewsCount'] })
    expect(JSON.parse(response.body).dig('data', 0, 'TrackedEvents.pageViewsCount')).to eq('12')
  end

  it 'returns the correct number of tracked pageviews when only known users are allowed' do
    response = cube_query({ "measures": ['TrackedEvents.pageViewsCount'], "segments": ['TrackedEvents.knownUsers'] })

    expect(JSON.parse(response.body).dig('data', 0, 'TrackedEvents.pageViewsCount')).to eq('6')
  end

  it 'returns the correct number of unique users' do
    response = cube_query({ "measures": ['TrackedEvents.uniqueUsersCount'] })

    expect(JSON.parse(response.body).dig('data', 0, 'TrackedEvents.uniqueUsersCount')).to eq('11')
  end
end