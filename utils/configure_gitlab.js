const path = require('path');
const fs = require('fs');
const execSync = require('child_process').execSync;

const gdkDir = process.argv[2];

if (!gdkDir) {
    console.log('Please provide the path to your Gitlab install (i.e. node configure_gitlab.js ./path/to/gdk/gitlab');
    process.exit();
}

const absGdkPath = path.resolve(gdkDir);

const cubeEnvFile = fs.readFileSync('cube/.env').toString();
const cubeEnv = cubeEnvFile.split('\n').reduce((map, line) => {
    const [key, value] = line.split('=');
    map[key] = value;
    return map;
}, {});

const appSettings = `ApplicationSetting.update(
    product_analytics_enabled: true,
    product_analytics_data_collector_host: 'http://localhost:9091',
    cube_api_base_url: 'http://localhost:4000',
    cube_api_key: '${cubeEnv['CUBEJS_API_SECRET']}',
    product_analytics_configurator_connection_string: 'http://test:test@localhost:4567'
)`

console.log(`Writing the following to your GDK Gitlab config ${appSettings}`);

execSync(`cd ${absGdkPath}; bundle exec rails runner "${appSettings}"`)

console.log('Configuration has been updated!');
