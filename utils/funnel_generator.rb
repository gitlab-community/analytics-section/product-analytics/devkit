# frozen_string_literal: true

require 'csv'
require 'securerandom'

@doc_encodings = ['utf-8', 'windows-1252']
@browsers = ["Chrome", "Firefox", "Safari", "Edge", "Opera", "IE"]
@versions = ['1', '2', '3']

how_many_users = 100

user_ids = (1..how_many_users).map { |i| SecureRandom.hex(10) }
def event(time, doc_path, user_id)
  [time.strftime('%Y-%m-%d %H:%M:%S'),'e3d7e404-c278-4cd5-91de-b5a675612759', @doc_encodings.sample, 'example.com', doc_path,'','pageview', SecureRandom.uuid, -60,"Page Title",'Apple','Mac','Mac','Mac OS X','10.15.7',@browsers.sample,@versions.sample,"http://example.com/#{doc_path}",'2560x1440','172.18.0.1','jitsu',"http://example.com/#{doc_path}","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36", user_id, user_id,'en-US', time.strftime('%Y-%m-%d %H:%M:%S'),'1113x1313',""]
end

# Generate completed funnels for each user
output_string = CSV.generate do |csv|
  user_ids.each do |user_id|
    csv << event(Time.now, '/page1.html', user_id)
    csv << event(Time.now + 20, '/page2.html', user_id)
    csv << event(Time.now + 100, '/page3.html', user_id)
  end

  user_ids.last(80).each do |user_id|
    csv << event(Time.now + 150, '/page4.html', user_id)
    csv << event(Time.now + 200, '/page5.html', user_id)
  end

  user_ids.last(12).each do |user_id|
    csv << event(Time.now + 300, '/page6.html', user_id)
    csv << event(Time.now + 350, '/page7.html', user_id)
  end
end





File.write("./utils/tmp_data.csv", output_string)
