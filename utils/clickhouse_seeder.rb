# Use this script to generate 1,000,000 events to add to clickhouse
# Run this script with `ruby clickhouse_seeder.rb`, and then import. (Seems to work well in JetBrains database tool)
# This script is not optimized for speed, but rather for readability. It's not meant to be run in production.

# On an M1 Max, this script takes about 2 minutes to run and about 10 minutes to import.

require 'csv'
require 'securerandom'

doc_encodings = ['utf-8', 'windows-1252']
browsers = ["Chrome", "Firefox", "Safari", "Edge", "Opera", "IE"]
versions = ['1', '2', '3']

user_ids = (1..15).map { |i| SecureRandom.hex(10) }

output_string = CSV.generate do |csv|
  user_ids.each do |user_id|
    10000.times do |i|
      csv << [(Time.now + (i)).strftime('%Y-%m-%d %H:%M:%S'),'e3d7e404-c278-4cd5-91de-b5a675612759', doc_encodings.sample, 'gitlab.com', '/devkit/example_projects/project_20/index.html','?_ijt=od28rciuft1hk8673ajjmqqt0u&_ij_reload=RELOAD_ON_SAVE','pageview', SecureRandom.uuid, -60,"Page Title",'Apple','Mac','Mac','Mac OS X','10.15.7',browsers.sample,versions.sample,'http://localhost:63342/devkit/example_projects/project_20/index.html?_ijt=od28rciuft1hk8673ajjmqqt0u&_ij_reload=RELOAD_ON_SAVE','2560x1440','172.18.0.1','jitsu','http://localhost:63342/devkit/example_projects/project_20/index.html?_ijt=od28rciuft1hk8673ajjmqqt0u&_ij_reload=RELOAD_ON_SAVE',"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36", user_id, user_id,'en-US', (Time.now + (i)).strftime('%Y-%m-%d %H:%M:%S'),'1113x1313',""]
    end
  end
end

File.write("./utils/tmp_data.csv", output_string)
